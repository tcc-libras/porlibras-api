'use strict';

const Helpers = require('./../helpers');
const { paginationValidator } = require('../../utils/validators/pagination');
const Joi = require('joi');

module.exports = Helpers.withDefaults({
    method: 'get',
    path: '/queremas',
    options: {
        tags: ['api'],
        validate: {
            query: {
                ...paginationValidator,
                type: Joi.number().integer().optional(),
                uuid: Joi.string().guid({
                    version: ['uuidv5']
                }).optional()
            }
        },
        handler: async (request) => {

            const { queremaService } = request.services();
            const { query } = request;
            return await queremaService.list(query);

        }
    }
});
