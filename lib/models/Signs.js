'use strict';

const Joi = require('joi');
const { Model } = require('./helpers');

module.exports = class Signs extends Model {
    static get tableName() {

        return 'Signs';
    }

    static get joiSchema() {

        return Joi.object({
            complexSignsId: Joi.number()
                .integer()
                .greater(0),
            simpleSignalsId: Joi.number()
                .integer()
                .greater(0)
        });
    }


    $parseDatabaseJson(json) {

        json = super.$parseDatabaseJson(json);



        return json;
    }
};
