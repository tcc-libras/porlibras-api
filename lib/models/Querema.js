'use strict';

const Joi = require('joi');
const { Model } = require('./helpers');
const UUID = require('uuid/v4');
const QueremaType = require('./QueremaType');

module.exports = class Querema extends Model {

    static get tableName() {

        return 'Queremas';
    }


    static get modifiers() {

        return {
            getColumns(query) {

                query.columns(
                    [
                        'id',
                        'name',
                        'type',
                        'picture_path',
                        'updated_at',
                        'uuid'
                    ]
                );
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer(),
            name: Joi.string(),
            type: Joi
                .number()
                .integer()
                .allow(null),
            picture_path: Joi.string(),
            createdAt: Joi.date().iso(),
            updatedAt: Joi.date().iso(),
            uuid: Joi.string().guid({
                version: ['uuidv5']
            })
        });
    }

    static get relationMappings() {

        return {
            queremaType: {
                relation: Model.BelongsToOneRelation,
                modelClass: QueremaType,
                join: {
                    from: 'Queremas.type',
                    to: 'QueremaTypes.id'
                }
            }
        };
    }



    $beforeInsert() {

        const now = new Date();

        this.created_at = now;
        this.updated_at = now;
        this.uuid = UUID();
    }

    $beforeUpdate() {

        const now = new Date();

        this.updated_at = now;
    }

    $parseDatabaseJson(json) {

        json = super.$parseDatabaseJson(json);

        json.created_at = json.created_at && new Date(json.created_at);
        json.updated_at = json.updated_at && new Date(json.updated_at);

        return json;
    }
};
