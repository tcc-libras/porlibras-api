'use strict';

const Joi = require('joi');
const Helpers = require('../../helpers');
// const FacialExpression = require('../../../models/FacialExpression');
// const HandMoviment = require('../../../models/HandMoviment');
// const HandOrientation = require('../../../models/HandOrientation');
// const HandPosition = require('../../../models/HandPosition');
// const ContactPoint = require('../../../models/ContactPoint');
const ComplexSignal = require('../../../models/ComplexSignal');

const SimpleSignalValidation = Joi.object().keys({
    id: Joi.number()
        .integer()
        .greater(0),
    shortDescription: Joi.string().required(),
    longDescription: Joi.string().allow('').optional()
    // handMovimentId: HandMoviment.field('id'),
    // handPositionId: HandPosition.field('id'),
    // handOrientationId: HandOrientation.field('id'),
    // facialExpressionId: FacialExpression.field('id'),
    // avatarId: ContactPoint.field('id')
});

module.exports = Helpers.withDefaults({
    method: 'put',
    path: '/signal/complex-signal/{uuid}',
    options: {
        tags: ['api'],
        validate: {
            params: {
                uuid: Joi.string().guid({
                    version: ['uuidv5']
                })
            },
            payload: {
                signal: Joi.object().keys({
                    shortDescription: ComplexSignal.field('shortDescription').required(),
                    longDescription: ComplexSignal.field('longDescription')
                        .allow('')
                        .optional(),
                    signs: Joi.array().items(SimpleSignalValidation)
                })
            }
        },
        handler: async (request, h) => {

            const { uuid } = request.params;
            const { signal: signalInfo } = request.payload;
            const { complexSignalService } = request.services();

            const updateAndFetchSignal = async (txn) => {

                const [signal] = await complexSignalService.findByUUID(uuid);
                const { id } = signal;

                return await complexSignalService.update(id, signalInfo, txn);
            };

            await h.context.transaction(updateAndFetchSignal);

            return {
                data: await complexSignalService.findByUUID(uuid)
            };
        }
    }
});
