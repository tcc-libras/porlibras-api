'use strict';

const TYPES = {
    FACIAL_EXPRESSION: {
        NAME: 'FacialExpressions',
        SLUG: 'Facial Expression',
        DESCRIPTION: '',
        ID: 1
    },
    HAND_MOVIMENT: {
        NAME: 'HandMoviments',
        SLUG: 'Hand Moviment',
        DESCRIPTION: '',
        ID: 2
    },
    HAND_ORIENTATION: {
        NAME: 'HandOrientations',
        SLUG: 'Hand Orientation',
        DESCRIPTION: '',
        ID: 3
    },
    HAND_POSITION: {
        NAME: 'HandPositions',
        SLUG: 'Hand Position',
        DESCRIPTION: '',
        ID: 4
    },
    // Location
    LOCATION: {
        NAME: 'Location',
        SLUG: 'location',
        DESCRIPTION: '',
        ID: 5
    },
    // MinorLocation
    MINOR_LOCATION: {
        NAME: 'MinorLocation',
        SLUG: 'Minor Location',
        DESCRIPTION: '',
        ID: 6
    },
    // MajorLocation
    MAJOR_LOCATION: {
        NAME: 'MajorLocation',
        SLUG: 'Major Location',
        DESCRIPTION: '',
        ID: 7
    },
    // ContactPoint
    CONTACT_POINT: {
        NAME: 'ContactPoint',
        SLUG: 'Contact Point',
        DESCRIPTION: '',
        ID: 8
    }
};
exports.TYPES = TYPES;

const PATHS = {
    FACIAL_EXPRESSION: 'facial-expression/',
    HAND_MOVIMENT: 'hand-moviments/',
    HAND_ORIENTATION: 'hand-orientation/',
    HAND_POSITION: 'hand-position/'
};
exports.PATHS = PATHS;
