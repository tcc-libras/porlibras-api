'use strict';

const Joi = require('joi');
const { Model } = require('../helpers');
const MajorLocation = require('./MajorLocation');
const MinorLocation = require('./MinorLocation');
const Uuid = require('uuid/v4');

module.exports = class Location extends Model {
    static get tableName() {

        return 'Location';
    }

    static get modifiers() {

        return {
            getColumns(query) {

                query.columns([
                    'id',
                    'uuid',
                    'updated_at',
                    'majorLocationId',
                    'minorLocationId'
                ]);
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer(),
            name: Joi.string(),
            majorLocationId: Joi
                .number()
                .integer(),
            minorLocationId: Joi
                .number()
                .integer()
                .empty('')
                .allow(null),
            createdAt: Joi.date().iso(),
            updatedAt: Joi.date().iso(),
            uuid: Joi.string().guid({
                version: ['uuidv4']
            })
        });
    }

    static get relationMappings() {

        return {
            majorLocation: {
                relation: Model.BelongsToOneRelation,
                modelClass: MajorLocation,
                join: {
                    from: 'Location.majorLocationId',
                    to: 'MajorLocation.id'
                }
            },
            minorLocation: {
                relation: Model.HasManyRelation,
                modelClass: MinorLocation,
                join: {
                    from: 'Location.minorLocationId',
                    to: 'MinorLocation.id'
                }
            }
        };
    }

    $beforeInsert() {

        const now = new Date();
        this.created_at = now;
        this.updated_at = now;
        this.uuid = Uuid();
    }

    $beforeUpdate() {

        const now = new Date();
        this.updated_at = now;
    }
};
