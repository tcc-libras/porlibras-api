'use strict';

const Schmervice = require('schmervice');

module.exports = class DisplayService extends Schmervice.Service {
    async list({ limit, offset }, txn) {

        const { SimpleSignal, ComplexSignal } = this.server.models();

        const simple = await SimpleSignal.query(txn)
            .select('id', 'uuid', 'shortDescription', 'longDescription')
            .withGraphFetched(
                '[facialExpression, handMoviment, handPosition, handOrientation, Avatar, FrontHand, BackHand]'
            )
            .limit(limit)
            .offset(offset)
            .orderBy('updated_at', 'desc');

        const complex = await ComplexSignal.query(txn)
            .select(
                'id',
                'uuid',
                'shortDescription',
                'longDescription',
                'updated_at'
            )
            .withGraphFetched(
                'signs.[facialExpression, handMoviment, handPosition, handOrientation, Avatar, FrontHand, BackHand]'
            )
            .limit(limit)
            .offset(offset)
            .orderBy('updated_at', 'desc');

        return [...complex, ...simple];
    }
};
