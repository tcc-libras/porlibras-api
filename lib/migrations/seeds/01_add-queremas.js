'use strict';

const { TYPES, PATHS } = require('../utils/constants');
const { generateFilenames, createQueremas } = require('../utils/utils');


exports.seed = (knex) => {

    // add hand position data
    const facialExpressionFilenames = generateFilenames({ numberOfFiles: 2 });
    const FacialExpressionQueremas = createQueremas({
        filenames: facialExpressionFilenames,
        path: PATHS.FACIAL_EXPRESSION,
        type: TYPES.FACIAL_EXPRESSION.ID
    });

    // add hand orientation data
    const handMovimentFilenames = generateFilenames({ numberOfFiles: 8 });
    const handMovimentnQueremas = createQueremas({
        filenames: handMovimentFilenames,
        path: PATHS.HAND_MOVIMENT,
        type: TYPES.HAND_MOVIMENT.ID
    });

    // add hand moviment data
    const handOrientationFilenames = generateFilenames({ numberOfFiles: 4 });
    const handOrientationQueremas = createQueremas({
        filenames: handOrientationFilenames,
        path: PATHS.HAND_ORIENTATION,
        type: TYPES.HAND_ORIENTATION.ID
    });

    // add facial expression data
    const handPositionFilenames = generateFilenames({ numberOfFiles: 450 });
    const handPositionQueremas = createQueremas({
        filenames: handPositionFilenames,
        path: PATHS.HAND_POSITION,
        type: TYPES.HAND_POSITION.ID
    });

    // add contact point data

    const data = [
        ...FacialExpressionQueremas,
        ...handMovimentnQueremas,
        ...handOrientationQueremas,
        ...handPositionQueremas
    ];

    return knex('Queremas').del().then(() => knex('Queremas').insert(data));
};
