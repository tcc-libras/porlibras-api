'use strict';

const Joi = require('joi');
const { Model } = require('./../helpers');
const MajorLocation = require('./MajorLocation');
const Uuid = require('uuid/v4');

module.exports = class MinorLocation extends Model {
    static get tableName() {

        return 'MinorLocation';
    }

    static get modifiers() {

        return {
            getColumns(query) {

                query.columns(['id', 'uuid', 'name', 'picture_path', 'updated_at']);
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer(),
            name: Joi.string(),
            majorLocationId: Joi
                .number()
                .integer(),
            picture_path: Joi.string(),
            createdAt: Joi.date().iso(),
            updatedAt: Joi.date().iso(),
            uuid: Joi.string().guid({
                version: ['uuidv4']
            })
        });
    }

    static get relationMappings() {

        return {
            majorLocation: {
                relation: Model.BelongsToOneRelation,
                modelClass: MajorLocation,
                join: {
                    from: 'MinorLocation.majorLocationId',
                    to: 'MajorLocation.id'
                }
            }
        };
    }

    $beforeInsert() {

        const now = new Date();
        this.created_at = now;
        this.updated_at = now;
        this.uuid = Uuid();
    }

    $beforeUpdate() {

        const now = new Date();
        this.updated_at = now;
    }
};
