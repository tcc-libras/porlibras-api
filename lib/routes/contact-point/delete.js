'use strict';

const Helpers = require('./../helpers');
const Joi = require('joi');

module.exports = Helpers.withDefaults({
    method: 'delete',
    path: '/contact-point/{uuid}',
    options: {
        tags: ['api'],
        validate: {
            params: {
                uuid: Joi.string().guid({
                    version: ['uuidv4']
                })
            }
        },
        handler: async (request, h) => {

            const { uuid } = request.params;
            const { contactPointService } = request.services();

            const deleteContactPoint = async (txn) => {

                return await contactPointService.delete({ uuid }, txn);
            };

            return await h.context.transaction(deleteContactPoint);

        }
    }
});
