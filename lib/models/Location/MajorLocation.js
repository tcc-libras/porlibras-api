'use strict';

const Joi = require('joi');
const { Model } = require('./../helpers');
const Uuid = require('uuid/v4');

module.exports = class MajorLocation extends Model {
    static get tableName() {

        return 'MajorLocation';
    }

    static get modifiers() {

        return {
            getColumns(query) {

                query.columns([
                    'id',
                    'uuid',
                    'name',
                    'picture_path',
                    'updated_at'
                ]);
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer(),
            name: Joi.string(),
            picture_path: Joi.string(),
            createdAt: Joi.date().iso(),
            updatedAt: Joi.date().iso(),
            uuid: Joi.string().guid({
                version: ['uuidv4']
            })
        });
    }

    static get relationMappings() { }

    $beforeInsert() {

        const now = new Date();
        this.created_at = now;
        this.updated_at = now;
        this.uuid = Uuid();
    }

    $beforeUpdate() {

        const now = new Date();
        this.updated_at = now;
    }
};
