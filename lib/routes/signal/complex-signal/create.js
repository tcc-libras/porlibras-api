'use strict';

const Joi = require('joi');
const Helpers = require('../../helpers');
const SimpleSignal = require('../../../models/SimpleSignal');
const ComplexSignal = require('../../../models/ComplexSignal');

const SimpleSignalValidation = Joi.object().keys({
    shortDescription: SimpleSignal.field('shortDescription').required(),
    longDescription: SimpleSignal.field('longDescription')
        .allow('')
        .optional(),
    handMoviment: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string()
                .guid({ version: ['uuidv5'] })
                .allow(null)
        })
        .optional(),
    handPosition: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string().guid({ version: ['uuidv5'] })
        })
        .optional(),
    handOrientation: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string().guid({ version: ['uuidv5'] })
        })
        .optional(),
    facialExpression: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string().guid({ version: ['uuidv5'] })
        })
        .optional(),
    avatar: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            bodyLocation: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string().guid({ version: ['uuidv5'] })
        })
        .optional(),
    frontHand: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            bodyLocation: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string().guid({ version: ['uuidv5'] })
        })
        .optional(),
    backHand: Joi.object()
        .keys({
            name: Joi.string().required(),
            picture_path: Joi.string().required(),
            bodyLocation: Joi.string().required(),
            updated_at: Joi.date().optional(),
            uuid: Joi.string().guid({ version: ['uuidv5'] })
        })
        .optional()
});

module.exports = Helpers.withDefaults({
    method: 'post',
    path: '/signal/complex-signal',
    options: {
        tags: ['api'],
        validate: {
            payload: {
                signal: Joi.object().keys({
                    shortDescription: ComplexSignal.field('shortDescription').required(),
                    longDescription: ComplexSignal.field('longDescription'),
                    signs: Joi.array().items(SimpleSignalValidation)
                })
            }
        },
        handler: async (request, h) => {

            const { signal: signalInfo } = request.payload;
            const { complexSignalService } = request.services();

            const createAndFetchSignal = async (txn) => {

                return await complexSignalService.create(signalInfo, txn);
            };

            return await h.context.transaction(createAndFetchSignal);
        }
    }
});
