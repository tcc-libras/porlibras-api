'use strict';

const Helpers = require('../../helpers');
const { paginationValidator } = require('../../../utils/validators/pagination');
const Joi = require('joi');

module.exports = Helpers.withDefaults({

    method: 'get',
    path: '/signal/simple-signal',
    options: {
        tags: ['api'],
        validate: {
            query: {
                ...paginationValidator,
                uuid: Joi.string().guid({
                    version: ['uuidv4']
                }),
                shortDescription: Joi.string(),
                videoURL: Joi.string()
                    .allow('')
                    .optional(),
                facialExpressionId: Joi.string()
                    .allow('')
                    .optional(),
                longDescription: Joi.string()
                    .allow('')
                    .optional(),
                handMovimentId: Joi.number()
                    .empty('')
                    .allow(null),
                handPositionId: Joi.number()
                    .integer()
                    .allow(null),
                handOrientationId: Joi.number()
                    .integer()
                    .allow(null),
                contactPointId: Joi.number()
                    .integer()
                    .allow(null)
            }
        },
        handler: async (request) => {

            const { query } = request;
            const { simpleSignalService } = request.services();

            return await simpleSignalService.fetch(query);
        }
    }
});
