'use strict';

const Joi = require('joi');
const Helpers = require('../../helpers');

module.exports = Helpers.withDefaults({
    method: 'patch',
    path: '/simple-signal/{uuid}',
    options: {
        tags: ['api'],
        validate: {
            params: {
                uuid: Joi.string().guid({
                    version: ['uuidv4']
                })
            },
            payload: {
                signal: Joi.object()
                    .required()
                    .keys({
                        shortDescription: Joi
                            .string()
                            .required(),
                        longDescription: Joi
                            .string()
                            .allow('')
                            .optional(),
                        HandActionType: Joi.string().optional(),
                        handMovimentId: Joi.number().allow(null),
                        handPositionId: Joi.number().allow(null),
                        handOrientationId: Joi.number().allow(null),
                        facialExpressionId: Joi.number().allow(null),
                        contactPointId: Joi.number().allow(null)
                    })
            }
        },
        handler: async (request, h) => {

            const { uuid } = request.params;
            const { signal } = request.payload;
            const { simpleSignalService } = request.services();

            const updateAndFetchSignal = async (txn) => {

                return await simpleSignalService.update({ uuid, signal }, txn);
            };

            return await h.context.transaction(updateAndFetchSignal);
        }
    }
});
