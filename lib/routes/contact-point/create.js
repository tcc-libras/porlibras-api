'use strict';

const Helpers = require('./../helpers');

module.exports = Helpers.withDefaults({
    method: 'post',
    path: '/contact-point',
    options: {
        tags: ['api'],
        payload: {
            allow: ['application/json', 'multipart/form-data'],
            defaultContentType: 'multipart/form-data',
            maxBytes: 1024 * 1024 * 5,
            output: 'file',
            parse: true
        },
        handler: async (request, h) => {
            /**
             * Needs use or create a location to define contact point
             * A major location needs a minor location
             */

            try {
                const {
                    contactPointService,
                    storageService: {
                        uploadFile,
                        createUniqFilename
                    }
                } = request.services();
                const {
                    payload: {
                        photo,
                        path,
                        queremaId,
                        locationId
                    }
                } = request;

                const extension = photo.headers['content-type'].split('/').pop();
                const gcsResponse = await uploadFile({
                    file: photo,
                    options: {
                        destination: `${path}/${createUniqFilename()}.${extension}`, //always unique
                        cacheControl: 'public, max-age=31536000'
                    }
                });

                const createAndFetchContactPoint = async (txn) => {

                    return await contactPointService.create({
                        filename: gcsResponse.file.metadata.name.split('/').pop(),
                        queremaId,
                        locationId,
                        path: gcsResponse.file.path
                    }, txn);
                };

                if (gcsResponse) {
                    return await h.context.transaction(createAndFetchContactPoint);
                }

            }
            catch (error) {
                console.log(error);
            }

        }
    }
});
