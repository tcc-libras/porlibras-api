'use strict';

const Helpers = require('../helpers');
const Joi = require('joi');
const { paginationValidator } = require('../../utils/validators/pagination');

const contactPointValidator = {
    id: Joi.number().integer().optional(),
    name: Joi.string().optional(),
    QueremaId: Joi
        .number()
        .integer()
        .allow(null).optional(),
    LocationId: Joi
        .number()
        .integer()
        .allow(null).optional(),
    picture_path: Joi.string().optional(),
    createdAt: Joi.date().iso().optional(),
    updatedAt: Joi.date().iso().optional(),
    uuid: Joi.string().guid({
        version: ['uuidv5']
    }).optional()
};

module.exports = Helpers.withDefaults({

    method: 'get',
    path: '/contact-point/{slug?}',
    options: {
        tags: ['api'],
        validate: {
            query: {
                ...paginationValidator,
                ...contactPointValidator
            }
        }
    },
    handler: async (request) => {

        const { contactPointService: service } = request.services();

        const {
            query
        } = request;


        return await service.fetch(query);
    }
});
