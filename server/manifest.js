'use strict';

const Dotenv = require('dotenv');
const Confidence = require('confidence');
const Toys = require('toys');

// Pull .env into process.env
Dotenv.config({ path: `${__dirname}/.env` });

// Glue manifest as a confidence store
module.exports = new Confidence.Store({
    server: {
        host: '0.0.0.0',
        port: process.env.PORT || 3000,
        debug: {
            $filter: 'NODE_ENV',
            development: {
                log: ['error', 'implementation', 'internal'],
                request: ['error', 'implementation', 'internal']
            },
            production: {
                request: ['implementation']
            }
        },
        routes: {
            cors: {
                $filter: 'NODE_ENV',
                development: true
            }
        }
    },
    register: {
        plugins: [
            {
                plugin: '../lib',
                routes: {
                    prefix: '/api'
                },
                options: {
                    jwtKey: {
                        $filter: { $env: 'NODE_ENV' },
                        $default: {
                            $env: process.env.APP_SECRET
                        },
                        production: {
                            $env: process.env.APP_SECRET
                        }
                    }
                }
            },
            {
                plugin: 'schwifty',
                options: {
                    $filter: 'NODE_ENV',
                    $default: {},
                    $base: {
                        migrateOnStart: true,
                        knex: {
                            client: 'pg',
                            useNullAsDefault: true,
                            connection: {
                                host: process.env.DB_HOST,
                                user: process.env.DB_USER,
                                password: process.env.DB_PASS,
                                database: process.env.DB_NAME,
                                port: process.env.DB_PORT,
                                charset: 'utf8'
                            },
                            poll: {
                                min: process.env.DB_POOL_MIN,
                                max: process.env.DB_POOL_MAX
                            },
                            seeds: { directory: 'lib/migrations/seeds' }
                        }
                    },
                    production: {
                        migrateOnStart: false
                    }
                }
            },
            {
                plugin: './plugins/swagger'
            },
            {
                plugin: {
                    $filter: { $env: 'NODE_ENV' },
                    $default: 'hpal-debug',
                    production: Toys.noop
                }
            }
        ]
    }
});
