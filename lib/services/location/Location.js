'use strict';

const Schmervice = require('schmervice');

module.exports = class LocationService extends Schmervice.Service {

    async fetchLocation(params, txn) {

        const { Location } = this.server.models();
        const { limit, offset, ...filter } = params;

        const query = Location
            .query(txn)
            .modify('getColumns')
            .withGraphFetched(`[
                    majorLocation(getColumns),
                    minorLocation(getColumns)
                ]`);
        if (Object.keys(filter).length > 0) {
            query.where(filter);
        }

        query
            .limit(limit)
            .offset(offset);

        const [data, total] = await Promise.all([
            query,
            Location
                .query(txn)
                .resultSize()
        ]);
        return { data, total };
    }

    async createLocation({ majorLocationId, minorLocationId }, txn) {

        const { Location } = this.server.models();
        const data = await Location
            .query(txn)
            .insertAndFetch({
                majorLocationId,
                minorLocationId
            });
        return data;
    }

    async updateLocation({ uuid, location }, txn) {

        const { Location } = this.server.models();
        try {
            const { id } = await Location.query(txn).throwIfNotFound().where('uuid', '=', uuid);
            return await Location.query(txn).patchAndFetchById(id, location);
        }
        catch (error) {
            console.log(error);
        }
    }

    async deleteLocation({ uuid }, txn) {

        const { Location } = this.server.models();
        try {
            const { id } = await Location.query(txn).findOne('uuid', '=', uuid);
            const affectedRows = await Location.query(txn).deleteById(id);
            return { affectedRows };
        }
        catch (error) {
            console.log(error);
        }
    }

    async fetchMajorLocation(params, txn) {

        const { MajorLocation } = this.server.models();

        const { limit, offset, ...filter } = params;
        const query = MajorLocation
            .query(txn)
            .modify('getColumns');

        if (Object.keys(filter).length > 0) {
            query.where(filter);
        }

        query
            .limit(limit)
            .offset(offset);

        const [data, total] = await Promise.all([
            query,
            MajorLocation
                .query(txn)
                .resultSize()
        ]);
        return { data, total };
    }

    async createMajorLocation({ filename, path }, txn) {

        const { MajorLocation } = this.server.models();
        const data = await MajorLocation
            .query(txn)
            .insertAndFetch({
                name: filename,
                picture_path: path
            });
        return data;
    }

    async updateMajorLocation({ uuid, majorLocation }, txn) {

        const { MajorLocation } = this.server.models();
        try {

            const { id, picture_path } = await MajorLocation.query(txn).findOne('uuid', '=', uuid);
            await this.deleteFileFromBucket(picture_path);
            return await MajorLocation.query(txn).patchAndFetchById(id, majorLocation);
        }
        catch (error) {
            console.log(error);
        }
    }

    async deleteMajorLocation({ uuid }, txn) {

        const { MajorLocation } = this.server.models();
        try {
            const { id, picture_path } = await MajorLocation.query(txn).findOne('uuid', '=', uuid);
            const affectedRows = await MajorLocation.query(txn).deleteById(id);

            if (affectedRows === 1) {
                await this.deleteFileFromBucket(picture_path);
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    async fetchMinorLocation(params, txn) {

        const { MinorLocation } = this.server.models();
        const { limit, offset, ...filter } = params;

        const query = MinorLocation
            .query(txn)
            .modify('getColumns');


        if (Object.keys(filter).length > 0) {
            query.where(filter);
        }

        query
            .limit(limit)
            .offset(offset);

        const [data, total] = await Promise.all([
            query,
            MinorLocation
                .query(txn)
                .resultSize()
        ]);

        return { data, total };
    }

    async createMinorLocation({ filename, path, majorLocationId }, txn) {

        const { MinorLocation } = this.server.models();
        const data = await MinorLocation
            .query(txn)
            .insertAndFetch({
                name: filename,
                majorLocationId,
                picture_path: path
            });
        return data;
    }

    async updateMinorLocation({ uuid, minorLocation }, txn) {

        const { MinorLocation } = this.server.models();
        try {

            const { id, picture_path } = await MinorLocation.query(txn).findOne('uuid', '=', uuid);
            await this.deleteFileFromBucket(picture_path);
            return await MinorLocation.query(txn).patchAndFetchById(id, minorLocation);
        }
        catch (error) {
            console.log(error);
        }
    }

    async deleteMinorLocation({ uuid }, txn) {

        const { MinorLocation } = this.server.models();
        try {
            const { id, picture_path } = await MinorLocation.query(txn).findOne('uuid', '=', uuid);
            const affectedRows = await MinorLocation.query(txn).deleteById(id);

            if (affectedRows === 1) {
                await this.deleteFileFromBucket(picture_path);
            }
        }
        catch (error) {
            console.log(error);
        }

    }

    async deleteFileFromBucket(picture_path) {

        const { storageService } = this.server.services();
        try {
            if (picture_path) {
                await storageService.deleteFile({ filename: picture_path });
            }
        }
        catch (error) {
            console.log(error);
        }
    }





};
