'use strict';

const Helpers = require('../../helpers');
const Joi = require('joi');

module.exports = Helpers.withDefaults({

    method: 'get',
    path: '/signal/complex-signal',
    options: {
        tags: ['api'],
        validate: {
            query: {
                limit: Joi.number().integer().min(1).default(20),
                offset: Joi.number().integer().min(0).default(0)
            }
        },
        handler: async (request) => {

            const { limit, offset } = request.query;
            const { complexSignalService } = request.services();

            return {
                data: await complexSignalService.list({ limit, offset })
            };
        }
    }
});
