'use strict';

const UUID = require('uuid/v5');

const createName = ({ item, extension = 'jpg', prefix = { repeat: 4, pattern: 0 } }) => {

    return `${item.toString().padStart(prefix.repeat, prefix.pattern)}.${extension}`;
};

const generateFilenames = ({ numberOfFiles, extension }) => {

    return Array(numberOfFiles)
        .fill()
        .map((_, idx) => createName({ item: idx + 1, extension }));
};

const createQueremas = ({ filenames, path, type }) => {

    return filenames.map((item) => {

        return {
            name: item,
            type,
            picture_path: `${path}${item}`,
            updated_at: new Date(),
            uuid: UUID(`${path}${item}`, UUID.URL)
        };
    });

};

exports.generateFilenames = generateFilenames;

exports.createQueremas = createQueremas;
