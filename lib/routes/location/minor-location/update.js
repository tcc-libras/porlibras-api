'use strict';

const Helpers = require('../../helpers');

module.exports = Helpers.withDefaults({
    method: 'patch',
    path: '/queremas/locations/minor/{uuid}',
    options: {
        tags: ['api'],
        payload: {
            allow: ['application/json', 'multipart/form-data'],
            defaultContentType: 'multipart/form-data',
            maxBytes: 1024 * 1024 * 5,
            output: 'file',
            parse: true
        },
        handler: async (request, h) => {

            try {
                const {
                    locationService,
                    storageService: {
                        uploadFile,
                        createUniqFilename
                    }
                } = request.services();
                const {
                    payload: {
                        photo,
                        path,
                        majorLocationId
                    }
                } = request;

                const { uuid } = request.params;

                let gcsResponse;
                if (photo) {
                    const extension = photo.headers['content-type'].split('/').pop();
                    try {
                        gcsResponse = await uploadFile({
                            file: photo,
                            options: {
                                destination: `${path}/${createUniqFilename()}.${extension}`, //always unique
                                cacheControl: 'public, max-age=31536000'
                            }
                        });
                    } catch (error) {
                        console.log(error);
                    }
                }

                const updateAndFetchMinorLocation = async (txn) => {

                    return await locationService.updateMinorLocation({
                        uuid,
                        minorLocation: {
                            ...(majorLocationId ? { majorLocationId } : {}),
                            ...(gcsResponse ? {
                                name: gcsResponse.file.metadata.name.split('/').pop(),
                                picture_path: gcsResponse.file.path
                            } : {})
                        }

                    }, txn);
                };

                return await h.context.transaction(updateAndFetchMinorLocation);


            }
            catch (error) {
                console.log(error);
            }

        }
    }
});
