Corrigir inserções utilizando insertGraph (está sendo realizada a criação duplicada de sinais simples)

### Redimensionar imagens

mogrify \
 -resize 300x300 \
 transparent \
 -gravity center \
 -extent 300x300 \
 -format jpg \
 -quality 75 \
 -path . \
 \*.jpg
