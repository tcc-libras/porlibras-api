'use strict';

const Helpers = require('./../helpers');

module.exports = Helpers.withDefaults({
    method: 'patch',
    path: '/queremas/{uuid}',
    options: {
        tags: ['api'],
        payload: {
            allow: ['application/json', 'multipart/form-data'],
            defaultContentType: 'multipart/form-data',
            maxBytes: 1024 * 1024 * 5,
            output: 'file',
            parse: true
        },
        handler: async (request, h) => {

            try {
                const { queremaService, storageService: { uploadFile, createUniqFilename } } = request.services();
                const {
                    payload: { photo, path, type },
                    params: { uuid }
                } = request;
                let gcsResponse;
                if (photo) {
                    const extension = photo.headers['content-type'].split('/').pop();
                    gcsResponse = await uploadFile({
                        file: photo,
                        options: {
                            destination: `${path}/${createUniqFilename()}.${extension}`, //always unique
                            cacheControl: 'public, max-age=31536000'
                        }
                    });
                }


                const updateAndFetchQuerema = async (txn) => {

                    return await queremaService.update({
                        uuid,
                        querema: {
                            ...(type ? { type } : {}),
                            ...(gcsResponse ? {
                                name: gcsResponse.file.metadata.name.split('/').pop(),
                                picture_path: gcsResponse.file.path
                            } : {})
                        }
                    }, txn);
                };

                return await h.context.transaction(updateAndFetchQuerema);
            }
            catch (error) {
                console.log(error);
            }

        }
    }
});
