'use strict';

const Helpers = require('../../helpers/');
const Joi = require('joi');

module.exports = Helpers.withDefaults({
    method: 'delete',
    path: '/queremas/locations/minor/{uuid}',
    options: {
        tags: ['api'],
        validate: {
            params: {
                uuid: Joi.string().guid({
                    version: ['uuidv4']
                })
            }
        },
        handler: async (request, h) => {

            const { uuid } = request.params;
            const { locationService } = request.services();

            const deleteMinorLocation = async (txn) => {

                return await locationService.deleteMinorLocation({ uuid }, txn);
            };

            return await h.context.transaction(deleteMinorLocation);

        }
    }
});
