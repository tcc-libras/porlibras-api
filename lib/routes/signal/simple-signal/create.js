'use strict';

const Joi = require('joi');
const Helpers = require('../../helpers');

module.exports = Helpers.withDefaults({
    method: 'post',
    path: '/simple-signal',
    options: {
        tags: ['api'],
        validate: {
            payload: {
                signal: Joi.object()
                    .required()
                    .keys({
                        shortDescription: Joi
                            .string()
                            .required(),
                        longDescription: Joi
                            .string()
                            .allow('')
                            .optional(),
                        HandActionType: Joi.string().required(),
                        handMovimentId: Joi.number().allow(null),
                        handPositionId: Joi.number().allow(null),
                        handOrientationId: Joi.number().allow(null),
                        facialExpressionId: Joi.number().allow(null),
                        contactPointId: Joi.number().allow(null)
                    })
            }
        },
        handler: async (request, h) => {

            const { signal } = request.payload;
            const { simpleSignalService } = request.services();

            const createAndFetchSignal = async (txn) => {

                return await simpleSignalService.create(signal, txn);
            };

            return await h.context.transaction(createAndFetchSignal);

        }
    }
});
