'use strict';

const Joi = require('joi');
const { Model } = require('./helpers');
const UUID = require('uuid/v4');
const Querema = require('./Querema');

module.exports = class SimpleSignal extends Model {

    static get tableName() {

        return 'SimpleSignals';
    }

    static get modifiers() {

        return {
            getColumns(query) {

                query
                    .columns(
                        [
                            'id',
                            'shortDescription',
                            'longDescription',
                            'videoURL',
                            'HandActionType',
                            'updated_at',
                            'uuid'
                        ]
                    );
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            uuid: Joi.string().guid({
                version: ['uuidv4']
            }),
            createdAt: Joi.date(),
            updatedAt: Joi.date(),
            shortDescription: Joi
                .string()
                .required(),
            longDescription: Joi
                .string()
                .allow('')
                .optional(),
            HandActionType: Joi
                .string()
                .required(),
            videoURL: Joi
                .string()
                .optional(),
            facialExpressionId: Joi
                .number()
                .integer()
                .allow(null),
            handMovimentId: Joi
                .number()
                .integer()
                .allow(null),
            handPositionId: Joi
                .number()
                .integer()
                .allow(null),
            handOrientationId: Joi
                .number()
                .integer()
                .allow(null),
            contactPointId: Joi
                .number()
                .integer()
                .allow(null)
        });
    }

    static get relationMappings() {


        return {
            facialExpression: {
                relation: Model.BelongsToOneRelation,
                modelClass: Querema,
                join: {
                    from: 'SimpleSignals.facialExpressionId',
                    to: 'Queremas.id'
                }
            },
            handMoviment: {
                relation: Model.BelongsToOneRelation,
                modelClass: Querema,
                join: {
                    from: 'SimpleSignals.handMovimentId',
                    to: 'Queremas.id'
                }
            },
            handPosition: {
                relation: Model.BelongsToOneRelation,
                modelClass: Querema,
                join: {
                    from: 'SimpleSignals.handPositionId',
                    to: 'Queremas.id'
                }
            },
            handOrientation: {
                relation: Model.BelongsToOneRelation,
                modelClass: Querema,
                join: {
                    from: 'SimpleSignals.handOrientationId',
                    to: 'Queremas.id'
                }
            },
            contactPoint: {
                relation: Model.BelongsToOneRelation,
                modelClass: Querema,
                join: {
                    from: 'SimpleSignals.contactPointId',
                    to: 'Queremas.id'
                }
            }
        };
    }

    $beforeInsert() {

        const now = new Date();

        this.created_at = now;
        this.updated_at = now;
        this.uuid = UUID();
    }

    $beforeUpdate() {

        const now = new Date();

        this.updated_at = now;
    }

    $parseDatabaseJson(json) {

        json = super.$parseDatabaseJson(json);

        json.created_at = json.created_at && new Date(json.created_at);
        json.updated_at = json.updated_at && new Date(json.updated_at);

        return json;
    }
};
