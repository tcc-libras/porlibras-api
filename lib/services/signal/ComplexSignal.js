'use strict';

const Schmervice = require('schmervice');

module.exports = class ComplexSignalService extends Schmervice.Service {

    async findById(id, txn) {

        const { ComplexSignal } = this.server.models();

        return await ComplexSignal.query(txn)
            .throwIfNotFound()
            .findById(id)
            .withGraphFetched('signs');
    }

    async findByUUID(uuid, txn) {

        const { ComplexSignal } = this.server.models();

        return await ComplexSignal.query(txn)
            .select(
                'id',
                'uuid',
                'shortDescription',
                'longDescription',
                'updated_at'
            )
            .withGraphFetched(
                'signs.[facialExpression, handMoviment, handPosition, handOrientation, Avatar, FrontHand, BackHand]'
            )
            .where('uuid', uuid);
    }

    async find({ signal, limit, offset }, txn) {

        const { ComplexSignal } = this.server.models();

        const query = ComplexSignal.query(txn);

        if (signal) {
            query.innerJoinRelation('signals').where('signal.name', signal);
        }

        const [signals, total] = await Promise.all([
            query
                .limit(limit)
                .offset(offset)
                .orderBy('createdAt', 'desc'),
            query.resultSize()
        ]);

        return { signals, total };
    }

    async list({ limit, offset }, txn) {

        const { ComplexSignal } = this.server.models();
        const query = ComplexSignal.query(txn);

        const [data, total] = await Promise.all([
            query
                .select(
                    'id',
                    'uuid',
                    'shortDescription',
                    'longDescription',
                    'updated_at'
                )
                .withGraphFetched(
                    'signs.[facialExpression, handMoviment, handPosition, handOrientation, Avatar, FrontHand, BackHand]'
                )
                .limit(limit)
                .offset(offset)
                .orderBy('updated_at', 'desc'),
            query.resultSize()
        ]);

        return { data, total };
    }

    async findSimpleSignal(signal, txn) {

        const { SimpleSignal } = this.server.models();
        const {
            shortDescription,
            longDescription,
            handPosition,
            handOrientation,
            facialExpression,
            handMoviment,
            Avatar,
            FrontHand,
            BackHand
        } = signal;

        const query = SimpleSignal.query(txn);

        if (handPosition) {
            query
                .innerJoinRelation('handPosition')
                .where('handPosition.id', handPosition.id);
        }

        if (handOrientation) {
            query
                .innerJoinRelation('handOrientation')
                .where('handOrientation.id', handOrientation.id);
        }

        if (facialExpression) {
            query
                .innerJoinRelation('facialExpression')
                .where('facialExpression.id', facialExpression.id);
        }

        if (handMoviment) {
            query
                .innerJoinRelation('handMoviment')
                .where('handMoviment.id', handMoviment.id);
        }

        if (Avatar) {
            query
                .innerJoinRelation('Avatar')
                .where('avatarId', Avatar.id);
        }

        if (FrontHand) {
            query
                .innerJoinRelation('FrontHand')
                .where('frontHandId', FrontHand.id);
        }

        if (BackHand) {
            query
                .innerJoinRelation('BackHand')
                .where('backHandId', BackHand.id);
        }

        query.where('shortDescription', 'like', `%${shortDescription}%`);

        if (longDescription && longDescription.length !== 0) {
            query.where('longDescription', 'like', `%${longDescription}%`);
        }

        return await query;
    }

    async create({ ...complexSignalInfo }, txn) {

        const { ComplexSignal, Signs } = this.server.models();

        const { shortDescription, longDescription, signs } = complexSignalInfo;

        const insertedComplexSignal = await ComplexSignal.query(txn).insertAndFetch(
            {
                shortDescription,
                longDescription
            }
        );

        const searchResult = await Promise.all(

            signs.map(async (item) => await this.findSimpleSignal(item))
        );

        const founds = searchResult.flat(1);
        const insertSigns = founds.map(async (item) => {

            return await Signs.transaction(async (transaction) => {

                return await Signs.query(transaction).insertAndFetch({
                    complexSignsId: insertedComplexSignal.id,
                    simpleSignalsId: item.id
                });
            });
        });

        return insertSigns;
    }

    async update({ uuid, signal }, txn) {

        const { ComplexSignal } = this.server.models();

        const query = await ComplexSignal.query(txn);

        return query;
    }
};
