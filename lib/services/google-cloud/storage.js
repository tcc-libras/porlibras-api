'use strict';

const Schmervice = require('schmervice');
const UUID = require('uuid/v4');
const { Storage } = require('@google-cloud/storage');

const projectId = process.env.GCS_PROJECT_ID;
const BUCKET = process.env.GCS_BUCKET_NAME;
const clientEmail = process.env.GCS_CLIENT_EMAIL;
const privateKey = process.env.GCS_PRIVATE_KEY.replace(/\\n/gm, '\n');

const GCS_CONFIG = {
    projectId,
    credentials: {
        client_email: clientEmail,
        private_key: privateKey
    }
};

const GStorage = new Storage(GCS_CONFIG);

module.exports = class StorageService extends Schmervice.Service {
    async listBuckets() {

        try {
            const [buckets] = await GStorage.getBuckets();
            const bucketNames = buckets.map((b) => b.name);

            return { status: 200, data: bucketNames };
        }
        catch (error) {
            console.log(error);
        }
    }

    /**
   *
   * @param {*} bucketName Nome do bucket utilizado
   * @param {*} options objeto contendo o prefixo e delimistador
   * {
   *      prefix: 'prefix',
   *      delimiter: '/'
   * }
   */
    async listFiles({ bucketName = BUCKET, options = {} }) {

        try {
            const [files] = await GStorage.bucket(bucketName).getFiles(options);
            const filenames = files.map((f) => f.name);
            return { status: 200, filenames };
        }
        catch (error) {
            console.log(error);
        }
    }

    createUniqFilename() {

        return UUID();
    }

    async uploadFile({ file, bucketName = BUCKET, options = {} }) {

        try {
            const filename = file.path;
            const config = options
                ? { ...options }
                : { cacheControl: 'public, max-age=31536000' };
            const [response] = await GStorage.bucket(bucketName).upload(
                filename,
                config
            );

            const {
                metadata: { id, name, bucket, contentType, size, timeCreated, updated },
                name: path
            } = response;

            return {
                file: {
                    path,
                    metadata: {
                        id,
                        name,
                        bucket,
                        contentType,
                        size,
                        timeCreated,
                        updated
                    }
                }
            };
        }
        catch (error) {
            console.log(error);
        }
    }

    async deleteFile({ filename, bucketName = BUCKET }) {

        try {
            await GStorage.bucket(bucketName).file(filename).delete();
        }
        catch (error) {
            console.log(error);
        }
    }
};
