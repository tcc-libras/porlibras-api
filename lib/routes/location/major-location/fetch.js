'use strict';

const Helpers = require('../../helpers');
const { paginationValidator } = require('../../../utils/validators/pagination');
const Joi = require('joi');

const majorLocationValidatorSchema = {
    id: Joi.number().integer().optional(),
    name: Joi.string().optional(),
    picture_path: Joi.string().optional(),
    createdAt: Joi.date().iso().optional(),
    updatedAt: Joi.date().iso().optional(),
    uuid: Joi.string().guid({
        version: ['uuidv4']
    }).optional()
};


module.exports = Helpers.withDefaults({

    method: 'get',
    path: '/queremas/locations/major',
    options: {
        tags: ['api'],
        validate: {
            query: {
                ...paginationValidator,
                ...majorLocationValidatorSchema

            }
        }
    },
    handler: async (request) => {

        const { locationService: service } = request.services();

        const {
            query
        } = request;
        return await service.fetchMajorLocation(query);

    }
});
