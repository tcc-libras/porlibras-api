'use strict';

const Joi = require('joi');
const { Model } = require('./helpers');
const UUID = require('uuid/v5');
const { deburr, kebabCase } = require('lodash');

module.exports = class QueremaType extends Model {

    static get tableName() {

        return 'QueremaTypes';
    }


    static get modifiers() {

        return {
            getColumns(query) {

                query.columns(
                    [
                        'id',
                        'type',
                        'slug',
                        'description',
                        'updated_at',
                        'uuid'
                    ]
                );
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer(),
            type: Joi.string(),
            slug: Joi.string(),
            description: Joi.string(),
            createdAt: Joi.date().iso(),
            updatedAt: Joi.date().iso(),
            uuid: Joi.string().guid({
                version: ['uuidv5']
            })
        });
    }

    async $beforeInsert(ctx) {

        const now = new Date();

        this.created_at = now;
        this.updated_at = now;
        this.uuid = UUID();
        await this._setSlug(ctx.transaction);
    }

    async $beforeUpdate(opt, ctx) {

        const now = new Date();

        this.updated_at = now;
        await this._setSlug(ctx.transaction);
    }

    $parseDatabaseJson(json) {

        json = super.$parseDatabaseJson(json);

        json.created_at = json.created_at && new Date(json.created_at);
        json.updated_at = json.updated_at && new Date(json.updated_at);

        return json;
    }

    async _setSlug(txn) {

        if (typeof this.type === 'undefined') {
            return;
        }

        const slug = kebabCase(deburr(this.type));
        const exists = await this.constructor.query(txn).where({ slug }).resultSize();

        this.slug = exists ? `${slug}-${Math.random().toString(36).substring(6)}` : slug;
    }
};
