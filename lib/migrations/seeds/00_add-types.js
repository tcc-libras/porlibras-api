'use strict';

const UUID = require('uuid/v5');
const { TYPES } = require('../utils/constants');
const { deburr, kebabCase } = require('lodash');

exports.seed = (knex) => {

    const data = [
        {
            type: TYPES.FACIAL_EXPRESSION.NAME,
            slug: kebabCase(deburr(TYPES.FACIAL_EXPRESSION.SLUG.toLowerCase())),
            description: TYPES.FACIAL_EXPRESSION.DESCRIPTION,
            uuid: UUID(TYPES.FACIAL_EXPRESSION.NAME, UUID.URL)
        },
        {
            type: TYPES.HAND_MOVIMENT.NAME,
            slug: kebabCase(deburr(TYPES.HAND_MOVIMENT.SLUG.toLowerCase())),
            description: TYPES.HAND_MOVIMENT.DESCRIPTION,
            uuid: UUID(TYPES.HAND_MOVIMENT.NAME, UUID.URL)
        },
        {
            type: TYPES.HAND_ORIENTATION.NAME,
            slug: kebabCase(deburr(TYPES.HAND_ORIENTATION.SLUG.toLowerCase())),
            description: TYPES.HAND_ORIENTATION.DESCRIPTION,
            uuid: UUID(TYPES.HAND_ORIENTATION.NAME, UUID.URL)
        },
        {
            type: TYPES.HAND_POSITION.NAME,
            slug: kebabCase(deburr(TYPES.HAND_POSITION.SLUG.toLowerCase())),
            description: TYPES.HAND_POSITION.DESCRIPTION,
            uuid: UUID(TYPES.HAND_POSITION.NAME, UUID.URL)
        },
        {
            type: TYPES.LOCATION.NAME,
            slug: kebabCase(deburr(TYPES.LOCATION.SLUG.toLowerCase())),
            description: TYPES.LOCATION.DESCRIPTION,
            uuid: UUID(TYPES.LOCATION.NAME, UUID.URL)
        },
        {
            type: TYPES.MAJOR_LOCATION.NAME,
            slug: kebabCase(deburr(TYPES.MAJOR_LOCATION.SLUG.toLowerCase())),
            description: TYPES.MAJOR_LOCATION.DESCRIPTION,
            uuid: UUID(TYPES.MAJOR_LOCATION.NAME, UUID.URL)
        },
        {
            type: TYPES.MINOR_LOCATION.NAME,
            slug: kebabCase(deburr(TYPES.MINOR_LOCATION.SLUG.toLowerCase())),
            description: TYPES.MINOR_LOCATION.DESCRIPTION,
            uuid: UUID(TYPES.MINOR_LOCATION.NAME, UUID.URL)
        },
        {
            type: TYPES.CONTACT_POINT.NAME,
            slug: kebabCase(deburr(TYPES.CONTACT_POINT.SLUG.toLowerCase())),
            description: TYPES.CONTACT_POINT.DESCRIPTION,
            uuid: UUID(TYPES.CONTACT_POINT.NAME, UUID.URL)
        }
    ];


    return knex('QueremaTypes').del().then(() => knex('QueremaTypes').insert(data));
};
