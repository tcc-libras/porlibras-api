'use strict';

const Schmervice = require('schmervice');

module.exports = class ContactPointService extends Schmervice.Service {

    async create({ filename, path, queremaId = null, locationId = null }, txn) {

        const { ContactPoint } = this.server.models();
        const data = await ContactPoint
            .query(txn)
            .insertAndFetch({
                name: filename,
                picture_path: path,
                QueremaId: queremaId,
                LocationId: locationId
            });

        return data;
    }

    async fetch(params, txn) {

        const { ContactPoint } = this.server.models();
        const { limit, offset, ...filter } = params;

        const query = ContactPoint
            .query(txn)
            .modify('getColumns')
            .withGraphFetched(`[
                Querema(getColumns), 
                Location(getColumns)
            ]`);

        if (Object.keys(filter).length > 0) {
            query.where(filter);
        }

        query
            .limit(limit)
            .offset(offset);

        const [data, total] = await Promise.all([
            query,
            ContactPoint
                .query(txn)
                .resultSize()
        ]);
        return { data, total };
    }

    async update({ uuid, contactPoint }, txn) {

        const { ContactPoint } = this.server.models();

        try {
            const { id, picture_path } = await ContactPoint.query(txn).findOne('uuid', '=', uuid);

            await this.deleteFileFromBucket(picture_path);

            return await ContactPoint.query(txn).patchAndFetchById(id, contactPoint);
        }
        catch (error) {
            console.log(error);
        }
    }

    async deleteFileFromBucket(picture_path) {

        const { storageService } = this.server.services();
        try {
            if (picture_path) {
                await storageService.deleteFile({ filename: picture_path });
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    async delete({ uuid }, txn) {

        const { ContactPoint } = this.server.models();

        try {
            const { id, picture_path: filename } = await ContactPoint
                .query(txn)
                .findOne('uuid', '=', uuid);

            const deletedRows = await ContactPoint.query(txn).deleteById(id);
            if (deletedRows === 1) {
                await this.deleteFileFromBucket(filename);
            }

            return { affectedRows: deletedRows };
        }
        catch (error) {
            console.log(error);
        }
    }
};
