'use strict';

const Schmervice = require('schmervice');

module.exports = class QueremaService extends Schmervice.Service {
    async findById(id, txn) {

        const { Querema } = this.server.models();
        return await Querema.query(txn).throwIfNotFound().findById(id);
    }

    async list(params, txn) {

        const { Querema } = this.server.models();
        const { limit, offset, ...filter } = params;

        const query = Querema.query(txn)
            .modify('getColumns')
            .orderBy('id', 'desc')
            .withGraphFetched(
                `[
                    queremaType(getColumns)
                ]`
            );

        if (Object.keys(filter).length > 0) {
            query.where(filter);
        }

        query
            .limit(limit)
            .offset(offset);

        const [data, total] = await Promise.all([
            query,
            query.resultSize()
        ]);

        return { data, total };
    }

    async listTypes(txn) {

        const { QueremaType } = this.server.models();
        const [data, total] = await Promise.all([
            QueremaType.query(txn),
            QueremaType.query(txn).resultSize()
        ]);

        return { data, total };
    }

    async findQueremaTypeByUUID({ uuid }, txn) {

        const { QueremaType } = this.server.models();
        return await QueremaType.query(txn).findOne('uuid', '=', uuid);
    }

    async filteredList({ uuid, limit, offset }, txn) {

        const { Querema } = this.server.models();

        const queremaType = await this.findQueremaTypeByUUID({ uuid });

        const [data, total] = await Promise.all([
            Querema.query(txn)
                .modify('getColumns')
                .where('type', queremaType.id)
                .orderBy('id', 'asc')
                .withGraphFetched(
                    `[
                    queremaType(getColumns)
                ]`
                )
                .limit(limit)
                .offset(offset),
            Querema.query(txn)
                .where('type', queremaType.id)
                .orderBy('id', 'asc')
                .resultSize()
        ]);

        return { data, total };
    }

    async create({ filename, type, path }, txn) {

        const { Querema } = this.server.models();
        const data = await Querema.query(txn).insertAndFetch({
            name: filename,
            type,
            picture_path: path
        });

        return data;
    }

    async update({ uuid, querema }, txn) {

        const { Querema } = this.server.models();
        try {
            const { id } = await Querema.query(txn).findOne('uuid', '=', uuid.toString());
            return await Querema.query(txn).patchAndFetchById(id, querema);
        }
        catch (error) {
            console.log(error);
        }
    }

    async listRelatedContactPoint({ id, limit, offset }, txn) {

        const { ContactPoint } = this.server.models();
        const [data, total] = await Promise.all([
            ContactPoint.query(txn)
                .modify('getColumns')
                .where('QueremaId', id)
                .withGraphFetched('[Querema(getColumns)]')
                .limit(limit)
                .offset(offset),
            ContactPoint.query(txn).where('QueremaId', id).resultSize()
        ]);
        return { data, total };
    }
};
