'use strict';

const Joi = require('joi');

const paginationValidator = {
    limit: Joi.number()
        .integer()
        .min(1)
        .default(10),
    offset: Joi.number()
        .integer()
        .min(0)
        .default(0)
};

module.exports = { paginationValidator };
