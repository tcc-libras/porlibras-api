'use strict';

const Helpers = require('../../helpers/');
const Joi = require('joi');

module.exports = Helpers.withDefaults({
    method: 'delete',
    path: '/queremas/locations/major/{uuid}',
    options: {
        tags: ['api'],
        validate: {
            params: {
                uuid: Joi.string().guid({
                    version: ['uuidv4']
                })
            }
        },
        handler: async (request, h) => {

            const { uuid } = request.params;
            const { locationService } = request.services();

            const deleteMajorLocation = async (txn) => {

                return await locationService.deleteMajorLocation({ uuid }, txn);
            };

            return await h.context.transaction(deleteMajorLocation);

        }
    }
});
