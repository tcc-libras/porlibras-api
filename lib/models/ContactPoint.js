'use strict';

const Joi = require('joi');
const { Model } = require('./helpers');
const Querema = require('./Querema');
const Location = require('./Location/Location');
const Uuid = require('uuid/v4');

module.exports = class ContactPoint extends Model {
    static get tableName() {

        return 'ContactPoint';
    }

    static get modifiers() {

        return {
            getColumns(query) {

                query.columns([
                    'id',
                    'name',
                    'picture_path',
                    'uuid',
                    'updated_at'
                ]);
            }
        };
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer(),
            name: Joi.string(),
            QueremaId: Joi
                .number()
                .integer()
                .allow(null),
            LocationId: Joi
                .number()
                .integer()
                .allow(null),
            picture_path: Joi.string(),
            createdAt: Joi.date().iso(),
            updatedAt: Joi.date().iso(),
            uuid: Joi.string().guid({
                version: ['uuidv5']
            })
        });
    }

    static get relationMappings() {

        return {
            Querema: {
                relation: Model.HasManyRelation,
                modelClass: Querema,
                join: {
                    from: 'ContactPoint.QueremaId',
                    to: 'Queremas.id'
                }
            },
            Location: {
                relation: Model.HasManyRelation,
                modelClass: Location,
                join: {
                    from: 'ContactPoint.LocationId',
                    to: 'Location.id'
                }
            }
        };
    }

    $beforeInsert() {

        const now = new Date();
        this.created_at = now;
        this.updated_at = now;
        this.uuid = Uuid();
    }

    $beforeUpdate() {

        const now = new Date();
        this.updated_at = now;
    }
};
