'use strict';

const Helpers = require('./../helpers');

module.exports = Helpers.withDefaults({
    method: 'get',
    path: '/queremas/types',
    options: {
        tags: ['api'],
        handler: async (request) => {

            const { queremaService } = request.services();
            return await queremaService.listTypes();
        }
    }
});
