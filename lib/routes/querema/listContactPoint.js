'use strict';

const Helpers = require('./../helpers');
const Joi = require('joi');
const { paginationValidator } = require('../../utils/validators/pagination');

module.exports = Helpers.withDefaults({
    method: 'get',
    path: '/queremas/{id}/contact-point',
    options: {
        tags: ['api'],
        validate: {
            query: {
                ...paginationValidator
            },
            params: Joi.object({
                id: Joi.number()
                    .integer()
            })
        },
        handler: async (request) => {

            const { queremaService } = request.services();

            const { limit, offset } = request.query;
            const { id } = request.params;

            return await queremaService.listRelatedContactPoint({
                id,
                limit,
                offset
            });

        }
    }
});
