'use strict';

const Joi = require('joi');
const Helpers = require('../../helpers');

module.exports = Helpers.withDefaults({

    method: 'get',
    path: '/signal/complex-signal/{uuid}',
    options: {
        tags: ['api'],
        validate: {
            params: {
                uuid: Joi.string().guid({
                    version: [
                        'uuidv4'
                    ]
                })
            }
        },
        handler: async (request) => {

            const { uuid } = request.params;
            const { complexSignalService } = request.services();

            return {
                data: await complexSignalService.findByUUID(uuid)
            };
        }
    }
});
