'use strict';

const Helpers = require('../helpers');

module.exports = Helpers.withDefaults({
    method: 'post',
    path: '/queremas/location',
    options: {
        tags: ['api'],
        handler: async (request, h) => {

            try {
                const { locationService } = request.services();
                const { payload: { majorLocationId, minorLocationId = null } } = request;
                const createAndFetchLocation = async (txn) => {

                    return await locationService.createLocation({
                        majorLocationId,
                        minorLocationId
                    }, txn);
                };

                return await h.context.transaction(createAndFetchLocation);

            }
            catch (error) {
                console.log(error);
            }

        }
    }
});
