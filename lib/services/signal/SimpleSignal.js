'use strict';

const Schmervice = require('schmervice');

module.exports = class SimpleSignalService extends Schmervice.Service {



    async list({ limit, offset }, txn) {

        const { SimpleSignal } = this.server.models();
        return await SimpleSignal.query(txn)
            .select('uuid', 'shortDescription', 'videoURL', 'longDescription', 'updated_at')
            .limit(limit)
            .offset(offset)
            .withGraphFetched(
                `[
                    facialExpression(getColumns), 
                    handMoviment(getColumns), 
                    handPosition(getColumns), 
                    handOrientation(getColumns), 
                    contactPoint(getColumns)
                    
                ]`
            )
            .orderBy('updated_at', 'desc');
    }

    async fetch(params, txn) {

        const { SimpleSignal } = this.server.models();
        const { limit, offset, ...filter } = params;

        const query = SimpleSignal
            .query(txn)
            .modify('getColumns')
            .withGraphFetched(
                `[
                    facialExpression(getColumns), 
                    handMoviment(getColumns), 
                    handPosition(getColumns), 
                    handOrientation(getColumns), 
                    contactPoint(getColumns)
                    
                ]`
            );

        const { shortDescription, longDescription, videoURL, uuid, ...ids } = filter;
        const filterParams = Object.entries(ids);

        if (shortDescription) {
            query.where('shortDescription', 'like', `${shortDescription}%`);
        }

        if (longDescription) {
            query.where('longDescription', 'like', `${longDescription}%`);
        }

        if (videoURL) {
            query.where('videoURL', 'like', `${videoURL}%`);
        }

        if (uuid) {
            query.where('uuid', 'like', `${uuid}%`);
        }


        if (filterParams) {
            filterParams.map((item) => {

                query.where(item);
            });
        }

        query
            .limit(limit)
            .offset(offset);

        const [data, total] = await Promise.all([
            query,
            SimpleSignal
                .query(txn)
                .resultSize()
        ]);
        return { data, total };
    }

    async create(simpleSignal, txn) {

        const { SimpleSignal } = this.server.models();

        return await SimpleSignal.query(txn)
            .insertAndFetch(simpleSignal);

    }

    async update({ uuid, signal }, txn) {

        const { SimpleSignal } = this.server.models();

        const { id } = await SimpleSignal.query(txn)
            .findOne('uuid', '=', uuid);

        return SimpleSignal.query(txn).patchAndFetchById(id, signal);


    }
};
