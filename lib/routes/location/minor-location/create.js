'use strict';

const Helpers = require('./../../helpers');

module.exports = Helpers.withDefaults({
    method: 'post',
    path: '/queremas/locations/minor',
    options: {
        tags: ['api'],
        payload: {
            allow: ['application/json', 'multipart/form-data'],
            defaultContentType: 'multipart/form-data',
            maxBytes: 1024 * 1024 * 5,
            output: 'file',
            parse: true
        },
        handler: async (request, h) => {

            try {
                const {
                    locationService,
                    storageService: {
                        uploadFile,
                        createUniqFilename
                    }
                } = request.services();
                const { payload: { file, path, majorLocationId } } = request;
                const extension = file.headers['content-type'].split('/').pop();

                const gcsResponse = await uploadFile({
                    file,
                    options: {
                        destination: `${path}/${createUniqFilename()}.${extension}`, //always unique
                        cacheControl: 'public, max-age=31536000'
                    }
                });

                const createAndFetchMinorLocation = async (txn) => {

                    return await locationService.createMinorLocation({
                        filename: gcsResponse.file.metadata.name.split('/').pop(),
                        path: gcsResponse.file.path,
                        majorLocationId
                    }, txn);
                };

                if (gcsResponse) {
                    return await h.context.transaction(createAndFetchMinorLocation);
                }

            }
            catch (error) {
                console.log(error);
            }

        }
    }
});
