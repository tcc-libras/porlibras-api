'use strict';

const Helpers = require('./../helpers');

module.exports = Helpers.withDefaults({
    method: 'post',
    path: '/queremas',
    options: {
        tags: ['api'],
        payload: {
            allow: ['application/json', 'multipart/form-data'],
            defaultContentType: 'multipart/form-data',
            maxBytes: 1024 * 1024 * 5,
            output: 'file',
            parse: true
        },
        handler: async (request, h) => {

            try {
                const { queremaService, storageService: { uploadFile, createUniqFilename } } = request.services();
                const { payload: { photo, path, type } } = request;
                const extension = photo.headers['content-type'].split('/').pop();
                const gcsResponse = await uploadFile({
                    file: photo,
                    options: {
                        destination: `${path}/${createUniqFilename()}.${extension}`, //always unique
                        cacheControl: 'public, max-age=31536000'
                    }
                });

                const createAndFetchQuerema = async (txn) => {

                    return await queremaService.create({
                        filename: gcsResponse.file.metadata.name.split('/').pop(),
                        type,
                        path: gcsResponse.file.path
                    }, txn);
                };

                if (gcsResponse) {
                    return await h.context.transaction(createAndFetchQuerema);
                }

            }
            catch (error) {
                console.log(error);
            }

        }
    }
});
