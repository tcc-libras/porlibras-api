'use strict';

const Joi = require('joi');
const { Model } = require('./helpers');
const UUID = require('uuid/v4');

module.exports = class ComplexSignal extends Model {

    static get tableName() {

        return 'ComplexSigns';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            shortDescription: Joi.string(),
            longDescription: Joi.string().allow('').optional(),
            uuid: Joi.string().guid({
                version: [
                    'uuidv4'
                ]
            }),
            created_at: Joi.date(),
            updated_at: Joi.date()
        });
    }

    static get relationMappings() {

        const SimpleSignal = require('./SimpleSignal');

        return {
            signs: {
                relation: Model.ManyToManyRelation,
                modelClass: SimpleSignal,
                join: {
                    from: 'ComplexSigns.id',
                    through: {
                        from: 'Signs.complexSignsId',
                        to: 'Signs.simpleSignalsId'
                    },
                    to: 'SimpleSignals.id'
                }
            }
        };
    }

    $beforeInsert() {

        const now = new Date();

        this.created_at = now;
        this.updated_at = now;
        this.uuid = UUID();
    }

    $beforeUpdate() {

        const now = new Date();

        this.updated_at = now;
    }

    $parseDatabaseJson(json) {

        json = super.$parseDatabaseJson(json);

        json.created_at = json.created_at && new Date(json.created_at);
        json.updated_at = json.updated_at && new Date(json.updated_at);

        return json;
    }
};
