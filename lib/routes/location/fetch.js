'use strict';

const Helpers = require('./../helpers');
const { paginationValidator } = require('../../utils/validators/pagination');

module.exports = Helpers.withDefaults({
    method: 'get',
    path: '/locations/{slug?}',
    options: {
        tags: ['api'],
        validate: {
            query: {
                ...paginationValidator
            }
        },
        handler: async (request) => {

            const { locationService: service } = request.services();
            const {
                query
            } = request;

            return await service.fetchLocation(query);
        }
    }
});
