
'use strict';

exports.up = async (knex) => {

    await knex.schema.createTable('QueremaTypes', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .string('type')
            .notNullable();
        table
            .string('slug')
            .notNullable();
        table
            .text('description')
            .nullable();
        table
            .uuid('uuid')
            .unique();
        table.timestamps(true, true);
    });

    await knex.schema.createTable('Queremas', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .string('name', 140)
            .nullable();
        table
            .integer('type')
            .references('id')
            .inTable('QueremaTypes')
            .nullable();
        table
            .text('picture_path')
            .notNullable();
        table
            .timestamps(true, true);
        table
            .uuid('uuid')
            .unique();
    });

    await knex.schema.createTable('MajorLocation', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .string('name');
        table
            .string('picture_path')
            .nullable();
        table
            .timestamps(true, true);
        table
            .uuid('uuid')
            .unique();
    });

    await knex.schema.createTable('MinorLocation', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .string('name');
        table
            .integer('majorLocationId')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('MajorLocation');
        table
            .string('picture_path')
            .nullable();
        table
            .timestamps(true, true);
        table
            .uuid('uuid')
            .unique();
    });

    await knex.schema.createTable('Location', (table) => {

        table
            .bigIncrements('id');
        table
            .integer('majorLocationId')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('MajorLocation')
            .index();
        table
            .integer('minorLocationId')
            .unsigned()
            .nullable()
            .references('id')
            .inTable('MinorLocation')
            .index()
            .onDelete('SET NULL');
        table
            .timestamps(true, true);
        table
            .uuid('uuid')
            .unique();
    });

    await knex.schema.createTable('ContactPoint', (table) => {

        table
            .bigIncrements('id');
        table
            .string('name');
        table
            .string('picture_path')
            .nullable();
        table
            .integer('QueremaId')
            .unsigned()
            .nullable()
            .references('id')
            .inTable('Queremas')
            .index();
        table
            .integer('LocationId')
            .unsigned()
            .nullable()
            .references('id')
            .inTable('Location')
            .index();
        table
            .uuid('uuid')
            .unique();
        table.timestamps(true, true);
    });

    await knex.schema.createTable('SimpleSignals', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .string('shortDescription', 250)
            .notNullable();
        table
            .text('longDescription')
            .nullable();
        table
            .string('videoURL', 255)
            .nullable();
        table.enu('HandActionType', ['ACTIVE', 'PASSIVE'], {
            useNative: true,
            existingType: true,
            enumName: 'HAND-ACTION-TYPE'
        });
        table
            .integer('facialExpressionId')
            .references('id')
            .inTable('Queremas');
        table
            .integer('handMovimentId')
            .references('id')
            .inTable('Queremas');
        table
            .integer('handOrientationId')
            .references('id')
            .inTable('Queremas');
        table
            .integer('handPositionId')
            .references('id')
            .inTable('Queremas');
        table
            .integer('contactPointId')
            .references('id')
            .inTable('Queremas');
        table
            .uuid('uuid')
            .unique();
        table
            .timestamps(true, true);
    });

    await knex.schema.createTable('ComplexSigns', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .string('shortDescription', 140)
            .notNullable();
        table
            .string('longDescription')
            .nullable();
        table
            .uuid('uuid')
            .unique();
        table
            .timestamps(true, true);
    });

    await knex.schema.createTable('Signs', (table) => {

        table
            .bigIncrements('id')
            .primary();
        table
            .integer('complexSignsId')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('ComplexSigns')
            .index();
        table
            .integer('simpleSignalsId')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('SimpleSignals')
            .index();
    });
};

exports.down = async (knex) => {

    await knex.schema.dropTableIfExists('Signs');
    await knex.schema.dropTableIfExists('ComplexSigns');
    await knex.schema.dropTableIfExists('SimpleSignals');
    await knex.schema.dropTableIfExists('ContactPoint');
    await knex.schema.dropTableIfExists('Location');
    await knex.schema.dropTableIfExists('MinorLocation');
    await knex.schema.dropTableIfExists('MajorLocation');
    await knex.schema.dropTableIfExists('Queremas');
    await knex.schema.dropTableIfExists('QueremaTypes');
};
